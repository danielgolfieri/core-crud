﻿using AutoMapper;
using CoreCrud.Domain.Models;
using CoreCrud.Domain.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CoreCrud.Api.Controllers
{
    [Route("api/Customer")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        [HttpGet]
        [Route("All")]
        public IEnumerable<CustomerViewModel> Get()
        {
            CustomerModel customer = new CustomerModel();
            var config = new MapperConfiguration(cfg => cfg.CreateMap<List<CustomerModel>, List<CustomerViewModel>>());
            IMapper iMapper = config.CreateMapper();
            IEnumerable<CustomerViewModel> dto = iMapper.Map<IEnumerable<CustomerModel>, IEnumerable<CustomerViewModel>>(customer.GetAll());
            return dto;
        }

        [HttpGet]
        public CustomerViewModel Get(Guid id)
        {
            CustomerModel customer = new CustomerModel(id);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<CustomerModel, CustomerViewModel>());
            IMapper iMapper = config.CreateMapper();
            CustomerViewModel dto = iMapper.Map<CustomerModel, CustomerViewModel>(customer.GetById());
            return dto;
        }

        [HttpPost]
        [Route("Register")]
        public IActionResult Post([FromBody]CustomerViewModel customerViewModel)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(500);
            }
            else
            {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<CustomerViewModel, CustomerModel>());
                IMapper iMapper = config.CreateMapper();
                CustomerModel customer = iMapper.Map<CustomerViewModel, CustomerModel>(customerViewModel);
                customer.Register();
                return StatusCode(200);
            }
        }

        [HttpPut]
        [Route("Update")]
        public IActionResult Put([FromBody]CustomerViewModel customerViewModel)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(500);
            }
            else
            {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<CustomerViewModel, CustomerModel>());
                IMapper iMapper = config.CreateMapper();
                CustomerModel customer = iMapper.Map<CustomerViewModel, CustomerModel>(customerViewModel);
                customer.Update();
                return StatusCode(200);
            }
        }

        [HttpDelete]
        public IActionResult Delete(Guid id)
        {
            CustomerModel customer = new CustomerModel(id);
            customer.Remove();
            return StatusCode(200);
        }
    }
}