﻿using CoreCrud.Domain.CoreModels;
using CoreCrud.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace CoreCrud.Domain.Models
{
    public class CustomerModel : Entity, ICustomerAppService
    {
        public CustomerModel() { }
        public CustomerModel(Guid id) { Id = id; }
        public CustomerModel(Guid id, string name, string email, DateTime birthDate)
        {
            Id = id;
            Name = name;
            Email = email;
            BirthDate = birthDate;
        }

        public string Name { get; private set; }

        public string Email { get; private set; }

        public DateTime BirthDate { get; private set; }

        public void Register()
        {
            //TODO: register new Customer
        }

        public IEnumerable<CustomerModel> GetAll()
        {
            //TODO: get all Customer
            return new List<CustomerModel>();
        }

        public void Remove()
        {
            //TODO: remove it
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public CustomerModel GetById()
        {
            //TODO: get by id
            return new CustomerModel()
            {
                Id = Id,
                Name = "Golfieri",
                Email = "danielgolfieri@gmail.com",
                BirthDate = DateTime.Now
            };
        }

        public void Update()
        {
            //TODO: update it Customer
        }
    }
}
