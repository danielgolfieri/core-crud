﻿using CoreCrud.Domain.Models;
using System;
using System.Collections.Generic;

namespace CoreCrud.Domain.Interfaces
{
    public interface ICustomerAppService : IDisposable
    {
        void Register();
        IEnumerable<CustomerModel> GetAll();
        CustomerModel GetById();
        void Update();
        void Remove();
    }
}
